# lirc_mqtt_gpio

An mqtt service for remote controlling devices through IR.

Overview
=

Any computer running linux, with gpio, can send infrared commands through lirc commands (irsend).

This simple python wrapper connects to an mqtt server and listen for commands that are matched to a lirc remote configuration.
If the command is a valid key for the configure remote are sent through an IR LED connected to the gpio.
This project aims at providing a simple service to run on an raspberry pi that can be controlled from homeassistant to automate and control any device through infrared (TV, hifi, aircon, etc)

Hardware
-

User -> homeassistant -> RPi -> GPIO -> IR LED -> TV

Installation
-

install:
 - lirc
 - virtualenv
 - python3

create virtualenv and install package

Configuration
-

Edit configuration ~/.lirc_mqtt_gpio/config.yml
set:
 - mqtt server
 - lirc file for remote
 - gpio settings


Set systemd service, enable, start 


Test
-

Run a command from any computer with the mosquitto client or from the mqtt HA panel:

mosquitto_publish ...

Troubleshooting
-

Check the logs under ~/.lirc_mqtt_gpio/messages.log

